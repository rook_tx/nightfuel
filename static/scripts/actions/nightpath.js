// ----------
// Detector
// ----------

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();


// ----------
// Vars
// ----------

var overlay = document.getElementById('overlay');

var w = window.innerWidth,
    h = window.innerHeight,
    mouseX = 0,
    mouseY = 0;

var container, scene, camera, renderer, clock, time = 0;

var rendererBackground = 0x020009;

var cameraZ = 10000;

var postprocessing = {},
    renderPass, wobblePass, glowPass, filmGrainPass;

var hueLower = 205,
    hueUpper = 280;

var radius = cameraZ*0.8,
    stars = Math.round(radius),
    starSize = radius/92;

var starSystem, uniforms, starsGeometry, texture;

var orbCount = 64,
    orb,
    orbs = [];
var trail, trailHeadGeometry, trailMaterial,
    trailLength = 120,
    trails = [];

var bpm = 136,
    bars = [],
    tilt = cameraZ*1.4,
    deltaX = 0,
    deltaY = 0,
    deltaZ = cameraZ*3.2,
    freqs = new Uint8Array( orbCount );

var jsNode, context, sourceNode,
    analyzerSmoothing = 0.4,
    analyzerResolution = 512,
    context = new (window.AudioContext || window.webkitAudioContext)(),
    gainNode = context.createGain(),
    muteButton = document.getElementById('mute'),
    unMuteButton = document.getElementById('unmute'),
    muted = false,
    playButton = document.getElementById('play'),
    paused = true;

var about = document.getElementById('about'),
    aboutSwitch = document.getElementById('aboutSwitch'),
    aboutClose = document.getElementById('aboutClose');


// ----------
// Mute
// ----------

muteButton.onclick = function(e) {
  e.preventDefault();
  if ( muted == false) {
    muted = true;
    muteButton.className = "mute active";
    unMuteButton.className = "mute inactive";
    gainNode.gain.value = 0;
  }
}

unMuteButton.onclick = function(e) {
  e.preventDefault();
  if ( muted == true ) {
    muted = false;
    muteButton.className = "mute inactive";
    unMuteButton.className = "mute active";
    gainNode.gain.value = 1;
  }
}


// ----------
// Play
// ----------

playButton.onclick = function(e) {
  e.preventDefault();
  if ( paused == false) {
    paused = true;
    playButton.className += " paused";
    context.suspend();
  } else {
    paused = false;
    playButton.className = "play";
    context.resume();
  }
}


// ----------
// About
// ----------

var hideAbout = new TimelineMax();
hideAbout.set('.about-overlay', {autoAlpha:0});
hideAbout.set('.about-copy h2', {autoAlpha:0,yPercent:100});
hideAbout.set('.about-copy p', {autoAlpha:0,yPercent:10});
hideAbout.set('.about-close', {xPercent:300});
if ( window.innerWidth > 640 ) {
  hideAbout.set('.profile-img', {xPercent:100});
} else {
  hideAbout.set('.profile-img', {autoAlpha:0})
}

var nav = document.getElementById('nav');

aboutSwitch.onclick = function(e) {
  e.preventDefault();
  var width = nav.offsetWidth - aboutSwitch.offsetWidth;
  about.className = "about about-open";
  var boutIt = new TimelineMax();
  boutIt.set('.about', { autoAlpha:1 });
  if ( window.innerWidth > 640 ) {
    boutIt.set('.about', { width: width });
    boutIt.to('#aboutSwitch span', 0.3, {autoAlpha:0, xPercent:-100, ease:Power3.easeOut}, 0);
  }
  boutIt.to('.about-overlay', 0.7, {autoAlpha:0.85, ease:Linear.easeNone}, 0);
  boutIt.to('.profile-img', 0.7, {autoAlpha:1,xPercent:0, ease:Power3.easeOut}, 0.1);
  boutIt.staggerTo(['.about-copy h2', '.about-copy p'], 1, {autoAlpha:1, yPercent:0, ease:Power3.easeOut}, 0.1, 0.2);
  boutIt.to('.about-close', 1, {xPercent:0, ease:Expo.easeOut}, 0.5);
}

aboutClose.onclick = function(e) {
  e.preventDefault();
  about.className = "about";
  var notBoutIt = new TimelineMax();
  notBoutIt.to('.about-overlay', 1, {autoAlpha:0, ease:Linear.easeNone}, 0);
  notBoutIt.to('.about-close', 0.5, {xPercent:300, ease:Power3.easeOut}, 0);
  notBoutIt.to('.about-copy p', 0.5, {autoAlpha:0, yPercent:10, ease:Power3.easeOut}, 0.1);
  notBoutIt.to('.about-copy h2', 0.5, {autoAlpha:0, yPercent:100, ease:Power3.easeOut}, 0.2);
  notBoutIt.to('.profile-img', 0.5, {autoAlpha:0, ease:Linear.easeNone}, 0.1);
  notBoutIt.to('#aboutSwitch span', 0.3, {autoAlpha:1, xPercent:0, ease:Power3.easeOut}, 0.5);
  if ( window.innerWidth > 640 ) {
    notBoutIt.set('.profile-img', {autoAlpha:1,xPercent:100});
  }
}



// ----------
// Intro
// ----------

// var nwidth = window.innerHeight * (26/800);
var nwidth = window.innerHeight * (10/800);
var nheight = window.innerHeight * (22/800);

var intro = new TimelineMax({paused:true, onComplete:setupRoutingGraph});
for ( var i = 0; i < 20; i ++ ) {
  intro.to('.stamp', 0.1, {autoAlpha:(0.75 + (Math.random() * 0.25))});
}
intro.to('.stamp', 0.1, {autoAlpha:1});
intro.to('.overlay', 3, {autoAlpha:0}, "-=2");

var introMob = new TimelineMax({paused:true, onComplete:setupRoutingGraph});
introMob.to('.overlay', 3, {autoAlpha:0});
introMob.fromTo('.wordmark', 0.75, {autoAlpha:0,x:-nwidth}, {autoAlpha:1,x:0}, 1.5);
introMob.staggerFromTo('.social-li', 1, {autoAlpha:0,y:64}, {autoAlpha:1,y:0,ease:Back.easeOut.config(0.75)}, 0.2, 1.5);
introMob.to('.preloader', 1, {autoAlpha:0});
introMob.fromTo('.monogram', 1, {autoAlpha:0}, {autoAlpha:1});

var begin = new TimelineMax({paused:true});
begin.to('.preloader', 1, {autoAlpha:0});
begin.fromTo('.wordmark', 0.75, {autoAlpha:0}, {autoAlpha:1});
begin.fromTo('.monogram', 1, {autoAlpha:0}, {autoAlpha:1}, "-=0.5");
begin.fromTo('.mute-wrap', 0.75, {autoAlpha:0,y:64}, {autoAlpha:1,y:0,ease:Back.easeOut.config(0.75)},"-=1");
begin.staggerFromTo('.social-li', 0.75, {autoAlpha:0,y:64}, {autoAlpha:1,y:0,ease:Back.easeOut.config(0.75)}, 0.1, "-=1");

var beginMob = new TimelineMax({paused:true});


// ----------
// init
// ----------

init();
animate();

function init() {
	container = document.getElementById( 'container' );

  initScene();
  initRenderer();
  initPostprocessing();

  initStars();
  initOrbs();
  initTrailRenderers();

  if (window.innerWidth > 640) {
    intro.play();
  } else {
    introMob.play();
  }

  document.addEventListener( 'mousemove', onDocumentMouseMove, false );
  // document.addEventListener( 'touchstart', onDocumentTouchStart, false );
  // document.addEventListener( 'touchmove', onDocumentTouchMove, false );
  window.addEventListener( 'resize', onWindowResize, false );

  window.addEventListener('blur', function() {
    clock.stop();
  });

  window.addEventListener('focus', function() {
    clock.start();
  });
}


// ----------
// events
// ----------

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}

function onDocumentMouseMove(e) {
  mouseX = e.clientX - (window.innerWidth/2);
  mouseY = e.clientY - (window.innerHeight/2);
}

function onDocumentTouchStart(e) {
	if ( e.touches.length > 1 ) {
		e.preventDefault();
		mouseX = e.touches[ 0 ].pageX - (window.innerWidth/2);
		mouseY = e.touches[ 0 ].pageY - (window.innerHeight/2);
	}
}

function onDocumentTouchMove(e) {
	if ( e.touches.length == 1 ) {
		e.preventDefault();
		mouseX = e.touches[ 0 ].pageX - (window.innerWidth/2);
		mouseY = e.touches[ 0 ].pageY - (window.innerHeight/2);
	}
}


// ----------
// Init scene and camera
// ----------

function initScene() {
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 1, 30000 );
  clock = new THREE.Clock();
  scene.add( camera );
  resetCamera();
}

function resetCamera() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  camera.position.set( 0, 0, deltaZ );
  camera.lookAt( scene.position );
}

function initRenderer() {
  renderer = new THREE.WebGLRenderer({ antialias: false });
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.setClearColor( rendererBackground );
  container.appendChild( renderer.domElement );
}


// ----------
// Stars
// ----------

function initStars( onFinished ) {
	var shaderMaterial = new THREE.ShaderMaterial( {
    uniforms: {
     color: {
       value: new THREE.Color( 0xffffff )
     },
    	texture: {
       value: new THREE.TextureLoader().load( "/static/images/textures/sprites/spark1.png" )
     }
    },
    vertexShader: ["attribute float size;", "attribute vec3 customColor;", "varying vec3 vColor;", "void main() {", "vColor = customColor;", "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );", "gl_PointSize = size * ( 300.0 / -mvPosition.z );", "gl_Position = projectionMatrix * mvPosition;", "}"].join("\n"),
    fragmentShader: ["uniform vec3 color;", "uniform sampler2D texture;", "varying vec3 vColor;", "void main() {", "gl_FragColor = vec4( color * vColor, 1.0 );", "gl_FragColor = gl_FragColor * texture2D( texture, gl_PointCoord );", "}"].join("\n"),
		blending:       THREE.AdditiveBlending,
		depthTest:      false,
		transparent:    true
	});

	var starsGeometry = new THREE.BufferGeometry();

	var positions = new Float32Array( stars * 3 );
	var colors = new Float32Array( stars * 3 );
	var sizes = new Float32Array( stars );

	var color = new THREE.Color();

	for ( var i = 0, i3 = 0; i < stars; i ++, i3 += 3 ) {
		positions[ i3 + 0 ] = ( Math.random() * 2 - 1 ) * radius;
		positions[ i3 + 1 ] = ( Math.random() * 2 - 1 ) * radius;
		positions[ i3 + 2 ] = ( Math.random() * 2 - 1 ) * radius;

    var hue = ( hueLower + (Math.random() * (hueUpper - hueLower)) ) / 360;

    color.setHSL( hue, (0.5 + (Math.random() * 0.5)), (0.5 + (Math.random() * 0.5)) );

		colors[ i3 + 0 ] = color.r;
		colors[ i3 + 1 ] = color.g;
		colors[ i3 + 2 ] = color.b;

		sizes[ i ] = starSize;
	}

	starsGeometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
	starsGeometry.addAttribute( 'customColor', new THREE.BufferAttribute( colors, 3 ) );
	starsGeometry.addAttribute( 'size', new THREE.BufferAttribute( sizes, 1 ) );

	starSystem = new THREE.Points( starsGeometry, shaderMaterial );

	scene.add( starSystem );

  if( onFinished ) {
    onFinished();
  }
}


// ----------
// Orbs & trails
// ----------

function initOrbs( onFinished ) {
  for ( var i = 0; i < orbCount; i ++ ) {
    var geometry = new THREE.SphereGeometry(1);
    var material = new THREE.MeshBasicMaterial({ color: rendererBackground });
    orb = new THREE.Mesh( geometry, material );
    orb.position.z = i*i+i*i - radius;
    orbs.push( orb );
    scene.add( orb );
  }
}

function initTrailRenderers( callback ) {
  for ( o = 0; o < orbs.length; o++ ) {
    var trailHeadGeometry = [];
    var twoPI = Math.PI * 2;
    var index = 0;
    var scale = ( o/orbCount )*10;
    var inc = twoPI / 32.0;

    for ( var i = 0; i <= twoPI + inc; i+= inc )  {
      var vector = new THREE.Vector3();
      vector.set( Math.cos( i ) * scale, Math.sin( i ) * scale, 0 );
      trailHeadGeometry[ index ] = vector;
      index ++;
    }

    var trail = new THREE.TrailRenderer( scene, false );
    trailMaterial = THREE.TrailRenderer.createBaseMaterial();
    trail.initialize( trailMaterial, trailLength, false, 0, trailHeadGeometry, orbs[o] );
    updateTrailColors(o);
    trail.activate();
    trails.push(trail);
  }

  if ( callback ) {
    callback();
  }
}

function updateTrailColors( o ) {
  var color = new THREE.Color();
  var hue = ( hueLower + ( Math.random() * (hueUpper - hueLower) )) / 360;
  color.setHSL( hue, (0.5 + (Math.random() * 0.5)), (0.4 + (Math.random() * 0.4)) );
  trailMaterial.uniforms.headColor.value.set( color.r, color.g, color.b, 1 );
}



// ----------
// Frequencies
// ----------

function setupRoutingGraph() {
  sourceNode  = context.createBufferSource();

  analyzer = context.createAnalyser();
  analyzer.smoothingTimeConstant = analyzerSmoothing;
  analyzer.fftSize = orbCount*2;

  jsNode = context.createScriptProcessor(analyzerResolution, 1, 1);

  jsNode.onaudioprocess = function() {
    analyzer.getByteFrequencyData( freqs );
  };

	jsNode.connect( context.destination );
	sourceNode.connect( analyzer );
	sourceNode.connect( gainNode );
	analyzer.connect( jsNode );
  gainNode.connect( context.destination );

	var req = new XMLHttpRequest();
	req.open('GET', '/static/audio/young.mp3', true);
	req.responseType = 'arraybuffer';

	req.onload = function() {
		context.decodeAudioData(req.response, function(buf) {
			sourceNode.buffer = buf;
			sourceNode.loop = true;
			sourceNode.start(0);
		}, function(e) {
      console.log(e);
    });
	};
	req.send();
}


// ----------
// Post
// ----------

function initPostprocessing() {
  renderPass = new THREE.RenderPass( scene, camera );
  var composer = new THREE.EffectComposer( renderer );

  glowPass = new THREE.ShaderPass(THREE.GlowShader);
  glowPass.uniforms.resolution.value = new THREE.Vector2( window.innerWidth, window.innerHeight );
  glowPass.uniforms.amount.value = 8;

  wobblePass = new THREE.ShaderPass(THREE.WobbleShader);
  wobblePass.uniforms.size.value = 8;

  filmGrainPass = new THREE.ShaderPass(THREE.FilmGrainShader);
  filmGrainPass.uniforms.resolution.value = new THREE.Vector2( window.innerWidth, window.innerHeight );
  filmGrainPass.renderToScreen = true;

  composer.addPass( renderPass );
  composer.addPass( wobblePass );
  composer.addPass( glowPass );
  composer.addPass( filmGrainPass );

  postprocessing.composer = composer;
}


// ----------
// Render
// ----------

function animate() {
  time =  clock.getElapsedTime() * 1000;
  var scaledTime = time * (bpm/60000);

  for ( i = 0; i < orbs.length; i++ ) {
    var ishift          = orbCount - i,
        ratio           = freqs[ishift] / 256,
        midThreshold    = 256 - (i/54 * 62),
        heavyThreshold  = 256 - ((orbCount - i)),
        orbRadius       = i * ( i + ( ratio * (i/15) )),
        orbRadiusMid    = i * ( i + ( ratio * (i/9) )),
        orbRadiusHeavy  = i * ( i + ( ratio * (i/2) )),
        orbZed          = i * ( i/4 ),
        theta           = scaledTime * i/95;

    if ( freqs[0] < 100 ) {
      orbRadius = i * i;
    }

    if ( freqs[ishift] > midThreshold ) {
      orbRadius = orbRadiusMid
    }

    if ( freqs[ishift] > heavyThreshold ) {
      orbRadius = orbRadiusHeavy
    }

    if ( i % 2 == 0 ) {
      orbs[i].position.x = orbRadius * Math.sin( theta );
      orbs[i].position.y = orbRadius * Math.cos( theta );
      orbs[i].position.z = i*i+i*i*(i/17) - radius*2 + ( orbZed * Math.cos( theta ) );
    } else {
      orbs[i].position.x = orbRadius * Math.cos( theta );
      orbs[i].position.y = orbRadius * Math.sin( theta );
      orbs[i].position.z = i*i+i*i*(i/17) - radius*2 - ( orbZed * Math.sin( theta ) );
    }

    trails[i].advance();
  }

  if ( freqs[0] > 100 ) {
    if ( window.innerWidth > 640 ) {
      begin.play();
    }
    deltaZ = cameraZ;
    deltaX = ( mouseX / window.innerWidth ) * tilt;
    deltaY = ( mouseY / window.innerHeight ) * tilt;
  }

  camera.position.x += ( deltaX - camera.position.x ) * 0.02;
  camera.position.y += ( deltaY - camera.position.y ) * 0.02;
  camera.position.z += ( deltaZ - camera.position.z ) * 0.03;
  camera.lookAt( scene.position );

  camera.rotation.z += scaledTime/4;

  filmGrainPass.uniforms.time.value = scaledTime;
  wobblePass.uniforms.time.value = scaledTime/2;
  wobblePass.uniforms.strength.value = 0.015 * Math.sin( scaledTime );

  requestAnimationFrame( animate );
  render();
}

function render() {
  postprocessing.composer.render();
}
