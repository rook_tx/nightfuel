/**
 * @author http://getmosh.io
 *
 */

 THREE.StarShader = {
  uniforms: {
    color: {
      value: new THREE.Color( 0xffffff )
    },
		texture: {
      value: new THREE.TextureLoader().load( "/static/images/textures/sprites/spark1.png" )
    }
  },
  vertexShader: ["attribute float size;", "attribute vec3 customColor;", "varying vec3 vColor;", "void main() {", "vColor = customColor;", "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );", "gl_PointSize = size * ( 300.0 / -mvPosition.z );", "gl_Position = projectionMatrix * mvPosition;", "}"].join("\n"),
  fragmentShader: ["uniform vec3 color;", "uniform sampler2D texture;", "varying vec3 vColor;", "void main() {", "gl_FragColor = vec4( color * vColor, 1.0 );", "gl_FragColor = gl_FragColor * texture2D( texture, gl_PointCoord );", "}"].join("\n")
};
