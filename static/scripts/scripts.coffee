Main = (()->

  init = ()->

    $(()->
      $c(document).trigger('Main.ready')
    )

    $c(document).on 'Main.ready', ()->
      $c(document).trigger('Main.update')

    $c(document).on 'click', '[data-nohref="true"]', (e)->
      e.preventDefault()

  return {
    init: init
  }

)()
