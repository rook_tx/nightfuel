# Nightfuel

Cactus prototype of Nightfuel landing page

### Set up

* Install Cactus: [github.com/eudicots/Cactus](https://github.com/eudicots/Cactus)

From project root:

* run `npm install`

* run `gulp`

* run `gulp watch`

* in new tab, run `cactus serve`

Navigate to [127.0.0.1:8000](http://127.0.0.1:8000)
